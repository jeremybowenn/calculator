package com.example.jeremy.calculator;

import com.udojava.evalex.Expression;

import java.math.BigDecimal;

/**
 * Created by Jeremy on 3/6/2018.
 */

public class Calculator {

    com.udojava.evalex.Expression expression;
    BigDecimal result;

    public Calculator(){}

    public void setExpression(String s){
        expression = new Expression(s);
    }

    public String eval() {
        try {
            result = expression.eval();
            return result.toString();
        }
        catch (Expression.ExpressionException e){
            return "ERROR";
        }
    }
}
