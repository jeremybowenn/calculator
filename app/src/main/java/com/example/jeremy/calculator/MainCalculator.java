package com.example.jeremy.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainCalculator extends AppCompatActivity {

    Calculator c;
    String expression;
    Button equals;
    Button Clear;

    Button Add;
    Button Sub;
    Button Multiply;
    Button Divide;

    Button Square;
    Button Sqrt;

    Button Sin;
    Button Cos;
    Button Tan;

    Button Open;
    Button Close;

    Button Zero;
    Button One;
    Button Two;
    Button Three;
    Button Four;
    Button Five;
    Button Six;
    Button Seven;
    Button Eight;
    Button Nine;

    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_calculator);

        c = new Calculator();
        text =  findViewById(R.id.txtvDisplay);
        equals = findViewById(R.id.btnEquals);
        Clear = findViewById(R.id.btnClear);

        Add = findViewById(R.id.btnAdd);
        Sub = findViewById(R.id.btnSub);
        Multiply = findViewById(R.id.btnMultiply);
        Divide = findViewById(R.id.btnDivide);

        Square = findViewById(R.id.btnSquared);
        Sqrt = findViewById(R.id.btnSqrRoot);

        Sin = findViewById(R.id.btnSin);
        Cos = findViewById(R.id.btnCos);
        Tan = findViewById(R.id.btnTan);

        Open = findViewById(R.id.btnOpen);
        Close = findViewById(R.id.btnClose);

        Zero = findViewById(R.id.btnZero);
        One = findViewById(R.id.btnOne);
        Two = findViewById(R.id.btnTwo);
        Three = findViewById(R.id.btnThree);
        Four = findViewById(R.id.btnFour);
        Five = findViewById(R.id.btnFive);
        Six = findViewById(R.id.btnSix);
        Seven = findViewById(R.id.btnSeven);
        Eight = findViewById(R.id.btnEight);
        Nine = findViewById(R.id.btnNine);

        /**
         * Gets the value in the display and sends it to the Calculator and displays the answer
         */
        equals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TextView display = findViewById(R.id.txtvDisplay);
                expression = (String) display.getText();
                if(!(expression.equals(""))) {
                    c.setExpression(expression);
                    display.setText(c.eval());
                }
            }
        });

        /**
         * Removes the symbol at the end of the text in the display
         */
        Clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                String temp = (String) text.getText();
                if(!(temp.compareTo("")==0)) {
                    temp = temp.substring(0, temp.length() - 1);
                    text.setText(temp);
                }

            }
        });

        /**
         * All the following methods call addToExpression with the appropriate symbol
         */
        Divide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                addToExpression("/");
            }
        });
        Multiply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                addToExpression("*");
            }
        });
        Sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                addToExpression("-");
            }
        });
        Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                addToExpression("+");
            }
        });



        Square.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                addToExpression("^");
            }
        });
        Sqrt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                addToExpression("sqrt(");
            }
        });



        Sin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                addToExpression("sin(");
            }
        });
        Cos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                addToExpression("cos(");
            }
        });
        Tan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                addToExpression("tan(");
            }
        });



        Open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                addToExpression("(");
            }
        });
        Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                addToExpression(")");
            }
        });



        Zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                addToExpression("0");
            }
        });

        One.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                addToExpression("1");
            }
        });

        Two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                addToExpression("2");
            }
        });

        Three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                addToExpression("3");
            }
        });

        Four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                addToExpression("4");
            }
        });

        Five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                addToExpression("5");
            }
        });

        Six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                addToExpression("6");
            }
        });

        Seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                addToExpression("7");
            }
        });

        Eight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                addToExpression("8");
            }
        });

        Nine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                addToExpression("9");
            }
        });
    }

    /**
     * Adds a string to the existing text in the display
     * @param add the String to add to the display
     */
    public void addToExpression(String add){
        String temp = (String) text.getText();
        temp = temp+add;
        text.setText(temp);

    }


}
